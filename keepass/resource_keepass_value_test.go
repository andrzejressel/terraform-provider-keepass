package keepass

import (
	"testing"

	"github.com/hashicorp/terraform/helper/resource"
	"github.com/hashicorp/terraform/terraform"
	"fmt"
)

func TestAccAWSBillingServiceAccount_basic(t *testing.T) {

	resource.UnitTest(t, resource.TestCase {
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccCheckAwsBillingServiceAccountConfig,
				// Check: resource.ComposeTestCheckFunc(
				// 	resource.TestCheckResourceAttr("data.keepass_value.bucket", "data", "User Name"),
				// ),
				Check:  testDataSourceGenericSecret_check,
			},
		},
	})
}

func testDataSourceGenericSecret_check(s *terraform.State) error {
	resourceState := s.Modules[0].Resources["data.keepass_value.bucket"]
	if resourceState == nil {
		return fmt.Errorf("resource not found in state %v", s.Modules[0].Resources)
	}

	iState := resourceState.Primary
	if iState == nil {
		return fmt.Errorf("resource has no primary instance")
	}

	wantUrl :=  "http://keepass.info/"
	if got, want := iState.Attributes["data.URL"], wantUrl; got != want {
		return fmt.Errorf("data contains %s; want %s", got, want)
	}

	return nil

}

const testAccCheckAwsBillingServiceAccountConfig = `
	provider "keepass" {
	  file = "/home/andrzej/NewDatabase.kdbx"
	  password = "test"
	}


	data "keepass_value" "bucket" {
	  database = "NewDatabase"
	  group = "General"
	  entry = "Sample Entry"
	}
`
