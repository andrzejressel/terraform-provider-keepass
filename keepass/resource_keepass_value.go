package keepass

import (
	"github.com/tobischo/gokeepasslib"
	"errors"
	"github.com/hashicorp/terraform/helper/schema"
)

func resourceKeepassValue() *schema.Resource {
	return &schema.Resource{
		Read: resourceKeepassValueRead,

		Schema: map[string]*schema.Schema{
			"database": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Database",
			},
			"group": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Group",
			},
			"entry": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Title",
			},
			"data": {
				Type:        schema.TypeMap,
				Computed:    true,
				Description: "Map of strings read from Vault.",
			},
		},
	}
}

func resourceKeepassValueRead(d *schema.ResourceData, meta interface{}) error {
	db := meta.(*gokeepasslib.Database)

	databaseName := d.Get("database").(string)
	groupName := d.Get("group").(string)
	entryTitle := d.Get("entry").(string)

	root := db.Content.Root

	var database *gokeepasslib.Group

	for _, e := range root.Groups {
		if e.Name == databaseName {
			database = &e
			break
		}
	}

	if database == nil {
		return errors.New("Database with name " + databaseName + " cannot be found")
	}

	var group *gokeepasslib.Group

	for _, e := range database.Groups {
		if e.Name == groupName {
			group = &e
			break
		}
	}

	if group == nil {
		return errors.New("Group with name " + groupName + " cannot be found")
	}

	var entry *gokeepasslib.Entry

	for _, e := range group.Entries {
		if e.GetTitle() == entryTitle {
			entry = &e
			break
		}
	}

	if entry == nil {
		return errors.New("Entry with title " + entryTitle + " cannot be found")
	}

	dataMap := map[string]string{}

	for _, e := range entry.Values {
		dataMap[e.Key] = e.Value.Content
	}

	d.SetId(entryTitle)

	d.Set("data", dataMap)

	return nil
}